package com.elarib;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.elarib.dao.RolesRepository;
import com.elarib.dao.UserInfoRepository;
import com.elarib.dao.UserRepository;
import com.elarib.entities.Component;
import com.elarib.entities.Role;
import com.elarib.entities.User;
import com.elarib.entities.UserInfo;
import com.elarib.entities.UserInfo.TypeInfo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CvAppApplication.class)
@WebAppConfiguration
public class testUser {

	@Autowired
	UserRepository userdao;

	@Autowired
	UserInfoRepository userInfo;
	
	@Autowired
	RolesRepository rolesDao;
	
	

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
//		UserInfo info1 = new UserInfo("Rue Ml Youssef", TypeInfo.HOME);
//		UserInfo info2 = new UserInfo("0600696848", TypeInfo.MOBILEPHONE);
//		UserInfo info3 = new UserInfo("me@elarib.com", TypeInfo.EMAIL);
//
//
//		User user = new User("EL ARIB", "Abdelhamide", 24, "Eleve Ingenieur", null, null);
//		userdao.save(user);
//		
//		
//		info1.setUser(user);
//		info2.setUser(user);
//		info3.setUser(user);
//		
//		userInfo.save(info1);
//		userInfo.save(info2);
//		userInfo.save(info3);
//		
//		Role role1 = new Role();
//		role1.setNomRole("ROLE_ADMIN");
//		role1.setUser(user);
//		rolesDao.save(role1);
		
		User user = userdao.findByEmail("elarib.abdelhamide@gmail.com");
		
		Set<Component> cp = user.getComponents();
		
		for(Component c : cp)
		{
			System.out.println(c.getID()+". "+c.getName());
		}

		
		
	}

}
