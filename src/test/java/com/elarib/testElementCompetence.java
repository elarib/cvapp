package com.elarib;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.elarib.dao.ElementCompetenceRepository;
import com.elarib.entities.ElementCompetence;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CvAppApplication.class)
@WebAppConfiguration
public class testElementCompetence {
	
	@Autowired
	ElementCompetenceRepository daoElt;
	
	

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		
		ElementCompetence elt = new ElementCompetence("css", "3 years programming", null);
		daoElt.save(elt);
	}

}
