package com.elarib;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.elarib.dao.UserRepository;
import com.elarib.dao.WorkExperienceRepository;
import com.elarib.entities.Education;
import com.elarib.entities.User;
import com.elarib.entities.WorkExperience;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CvAppApplication.class)
@WebAppConfiguration
public class testWorkExperience {

	
	@Autowired
	WorkExperienceRepository dao;
	
	@Autowired
	UserRepository userdao;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		User user = new User("EL ARIB", "Abdelhamide", 24, "Eleve Ingenieur", null, null);
		userdao.save(user);
		
		WorkExperience work = new WorkExperience("Work", "Senior Developer", 2016, 2020, "ferfr","SQLI");
		
		work.setUser(user);
		
		dao.save(work);
		
		
	}

}
