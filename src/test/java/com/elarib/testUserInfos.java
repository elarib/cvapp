package com.elarib;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.elarib.dao.ObjectifRepository;
import com.elarib.dao.UserInfoRepository;
import com.elarib.dao.UserRepository;
import com.elarib.entities.Objectif;
import com.elarib.entities.User;
import com.elarib.entities.UserInfo;
import com.elarib.entities.UserInfo.TypeInfo;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CvAppApplication.class)
@WebAppConfiguration

public class testUserInfos {

	@Autowired
	UserInfoRepository daoInfo;


	@Autowired
	UserRepository daoUser;


	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {

		User user = daoUser.findByEmail("me@elarib.com");
		

//		Objectif obj = new Objectif("objectif", "I'm Searching for an internship now");
//		obj.setUser(user);
//		daoObjectif.save(obj);
		
		TypeInfo linkedin = UserInfo.TypeInfo.LINKEDIN;
		
		UserInfo info = new UserInfo(   linkedin , "linkedin.com/elarib");
		
		info.setUser(user);
		
		daoInfo.save(info);
		
	

	}

}
