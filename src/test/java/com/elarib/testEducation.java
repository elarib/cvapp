package com.elarib;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.elarib.dao.EducationRepository;
import com.elarib.dao.UserRepository;
import com.elarib.entities.Education;
import com.elarib.entities.User;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CvAppApplication.class)
@WebAppConfiguration
public class testEducation {
	
	@Autowired
	EducationRepository dao;
	
	@Autowired
	UserRepository userdao;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		
		User user = userdao.findByEmail("elarib.abdelhamide@gmail.com");
		

		
		Education educ = new Education("Education", "INPT", 2013, 2016, "Eleve ingénieur ...");
		
		educ.setUser(user);
		
		dao.save(educ);
		
		
	}

}
