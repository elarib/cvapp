package com.elarib;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.elarib.dao.ObjectifRepository;
import com.elarib.dao.UserRepository;
import com.elarib.entities.Objectif;
import com.elarib.entities.User;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CvAppApplication.class)
@WebAppConfiguration

public class testObjectif {

	@Autowired
	ObjectifRepository daoObjectif;


	@Autowired
	UserRepository daoUser;


	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {

		User user = daoUser.findByEmail("elarib.abdelhamide@gmail.com");
		

//		Objectif obj = new Objectif("objectif", "I'm Searching for an internship now");
//		obj.setUser(user);
//		daoObjectif.save(obj);
		
		Objectif obj = daoObjectif.findByNameAndUser("objectif", user);
		
		System.out.println(obj.getID());

	}

}
