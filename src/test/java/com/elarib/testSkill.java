package com.elarib;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.elarib.dao.CompetenceRepository;
import com.elarib.dao.ElementCompetenceRepository;
import com.elarib.dao.SkillsRepository;
import com.elarib.dao.UserRepository;
import com.elarib.entities.Competence;
import com.elarib.entities.ElementCompetence;
import com.elarib.entities.Skill;
import com.elarib.entities.User;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CvAppApplication.class)
@WebAppConfiguration
public class testSkill {

	@Autowired
	ElementCompetenceRepository daoElt;

	@Autowired
	CompetenceRepository daoCptc;

	@Autowired
	UserRepository daoUser;

	@Autowired
	SkillsRepository daoSkill;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {

		User user = new User("EL ARIB", "Abdelhamide", 24, "Eleve Ingenieur", null, null);
		daoUser.save(user);

		Competence cptc = new Competence("Programming", null, null);

		daoCptc.save(cptc);

		ElementCompetence elt = new ElementCompetence("css", "3 years programming", null);
		elt.setCompetence(cptc);
		daoElt.save(elt);

		Skill skill = new Skill("Skill");
		daoSkill.save(skill);

		cptc.setSkills(new HashSet<Skill>() {
			{
				add(skill);
			}
		});
		daoCptc.save(cptc);

		
		
		
		Competence cptc2 = new Competence("Language", null, null);

		daoCptc.save(cptc2);

		ElementCompetence elt2 = new ElementCompetence("Francais", "3 years programming", null);
		elt2.setCompetence(cptc2);
		daoElt.save(elt2);
		
		
		cptc2.setSkills(new HashSet<Skill>() {
			{
				add(skill);
			}
		});
		daoCptc.save(cptc2);
		
	}

}
