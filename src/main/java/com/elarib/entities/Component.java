package com.elarib.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Component")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Component {

	public Component() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Component(String name) {
		super();
		this.name = name;
	}

	@Id
	@GeneratedValue
	protected Long ID;
	protected String name;

	@ManyToOne
	@JoinColumn(name="id_user")
	protected User user;
	
	public Long getID() {
		return this.ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}