package com.elarib.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ElementCompetence {
	@Id
	@GeneratedValue
	private Long ID;
	private String name;
	private String detail;

	
	
	public ElementCompetence(String name, String detail, Competence competence) {
		super();
		this.name = name;
		this.detail = detail;
		this.competence = competence;
	}

	@ManyToOne
	@JoinColumn(name = "id_Compentence")
	private Competence competence;

	public Long getID() {
		return this.ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return this.detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Competence getCompetence() {
		return competence;
	}

	public void setCompetence(Competence competence) {
		this.competence = competence;
	}

}