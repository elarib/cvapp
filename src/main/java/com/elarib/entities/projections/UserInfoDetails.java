package com.elarib.entities.projections;

import org.springframework.data.rest.core.config.Projection;

import com.elarib.entities.User;
import com.elarib.entities.UserInfo;
import com.elarib.entities.UserInfo.TypeInfo;

@Projection(name="details", types={UserInfo.class})
public interface UserInfoDetails {

	Long getID();
	TypeInfo getType();
	String getContent();
	User getUser();
	
	
	
	
}
