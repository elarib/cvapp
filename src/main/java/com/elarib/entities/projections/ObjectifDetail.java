package com.elarib.entities.projections;

import org.springframework.data.rest.core.config.Projection;

import com.elarib.entities.Objectif;
import com.elarib.entities.User;

@Projection(name="detail", types={Objectif.class})
public interface ObjectifDetail {
	
	Long getID();
	String getContent();
	User getUser();
	
	
	
}
