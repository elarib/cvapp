package com.elarib.entities.projections;

import org.springframework.data.rest.core.config.Projection;

import com.elarib.entities.Component;
import com.elarib.entities.User;

@Projection(name="detail", types={Component.class})
public interface ComponentDetail {
	
	Long getID();
	String getContent();
	User getUser();
	
	
	
}
