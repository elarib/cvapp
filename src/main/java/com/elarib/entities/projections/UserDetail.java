package com.elarib.entities.projections;

import org.springframework.data.rest.core.config.Projection;

import com.elarib.entities.User;

@Projection(name="UserDetail", types={User.class})
public interface UserDetail {

	Long getID();
	String getFirstName();
	String getLastName();
	String getDescription();
	String getEmail();
	
	
	
	
}
