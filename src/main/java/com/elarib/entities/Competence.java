package com.elarib.entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Competence {
	@Id
	@GeneratedValue
	private Long ID;
	private String name;
	
	@ManyToMany
	@JoinTable(name="competences_skills",joinColumns={@JoinColumn(name="Id_Competence",referencedColumnName="ID")},inverseJoinColumns={@JoinColumn(name="Id_Skill",referencedColumnName="ID")})
	private Set<Skill> skills;
	
	@OneToMany(mappedBy="competence")
	private Set<ElementCompetence> eltsCompetences;
	
	
	
	
	public Competence() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Competence(String name, Set<Skill> skills, Set<ElementCompetence> eltsCompetences) {
		super();
		this.name = name;
		this.skills = skills;
		this.eltsCompetences = eltsCompetences;
	}

	public Long getID() {
		return this.ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Skill> getSkills() {
		return skills;
	}

	public void setSkills(Set<Skill> skills) {
		this.skills = skills;
	}

	public Set<ElementCompetence> getEltsCompetences() {
		return eltsCompetences;
	}

	public void setEltsCompetences(Set<ElementCompetence> eltsCompetences) {
		this.eltsCompetences = eltsCompetences;
	}

}