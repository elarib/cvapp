package com.elarib.entities;

import javax.persistence.Entity;

@Entity
public class Objectif extends Component {

	private String content;
	
	
	public Objectif() {
		
		// TODO Auto-generated constructor stub
	}
	
	public Objectif(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public Objectif(String name, String content) {
		super(name);
		this.setContent(content);

	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Objectif [content=" + content + ", ID=" + ID + ", name=" + name + "]";
	}

	

}