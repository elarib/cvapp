package com.elarib.entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class Skill extends Component {

	public Skill() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Skill(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@ManyToMany(mappedBy="skills")
	private Set<Competence> competences;

	public Set<Competence> getCompetences() {
		return competences;
	}

	public void setCompetences(Set<Competence> competences) {
		this.competences = competences;
	}
}