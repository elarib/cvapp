package com.elarib.entities;

import javax.persistence.Entity;

@Entity
public class Education extends Component {

	
	public Education(){
		super();
	}
	
	public Education(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public Education(String name, String place, int yearFrom, int yearTo, String description) {
		super(name);
		this.place = place;
		this.yearFrom = yearFrom;
		this.yearTo = yearTo;
		this.description = description;
	}

	private String place;
	private int yearFrom;
	private int yearTo;
	private String description;

	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public int getYearFrom() {
		return this.yearFrom;
	}

	public void setYearFrom(int yearFrom) {
		this.yearFrom = yearFrom;
	}

	public int getYearTo() {
		return this.yearTo;
	}

	public void setYearTo(int yearTo) {
		this.yearTo = yearTo;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}