package com.elarib.entities;

import javax.persistence.Entity;

@Entity
public class WorkExperience extends Component {

	public WorkExperience(String name, String title, int yearFrom, int yearTo, String description, String place) {
		super(name);
		this.title = title;
		this.yearFrom = yearFrom;
		this.yearTo = yearTo;
		this.description = description;
		this.place = place;
	}

	public WorkExperience(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	private String title;
	private int yearFrom;
	private int yearTo;
	private String description;
	private String place;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYearFrom() {
		return this.yearFrom;
	}

	public void setYearFrom(int yearFrom) {
		this.yearFrom = yearFrom;
	}

	public int getYearTo() {
		return this.yearTo;
	}

	public void setYearTo(int yearTo) {
		this.yearTo = yearTo;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

}