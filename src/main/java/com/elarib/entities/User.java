package com.elarib.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"email"}))
public class User {

	@Id
	@GeneratedValue
	private Long ID;
	@Column(unique=true)
	private String email;
	private String password;
	@Transient
	private String confirmPassword;
	private String lastName;
	private String firstName;
	private int age;
	private String description;
	
	@Lob
	private byte[] photo;
	@OneToMany(fetch = FetchType.EAGER,mappedBy="user")
	private Set<UserInfo> infos;
	
	@OneToMany(mappedBy="user",fetch = FetchType.EAGER)
	private Set<Component> components;
	
	@OneToMany(fetch = FetchType.EAGER,cascade=CascadeType.REMOVE ,mappedBy="user")
	private Set<Role> roles;
	
	public Long getID() {
		return this.ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getPhoto() {
		return this.photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(String lastName, String firstName, int age, String description, byte[] photo, Set<UserInfo> infos) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.age = age;
		this.description = description;
		this.photo = photo;
		this.infos = infos;
	}

	public Set<UserInfo> getInfos() {
		return this.infos;
	}

	public void setInfos(Set<UserInfo> infos) {
		this.infos = infos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "User [ID=" + ID + ", email=" + email + ", password=" + password + ", lastName=" + lastName
				+ ", firstName=" + firstName + ", age=" + age + ", description=" + description + ", roles=" + roles
				+ "]";
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public Set<Component> getComponents() {
		return components;
	}

	public void setComponents(Set<Component> components) {
		this.components = components;
	}

	
}