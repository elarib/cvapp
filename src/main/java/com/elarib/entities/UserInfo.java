package com.elarib.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class UserInfo {

	public enum TypeInfo {
		HOME, EMAIL, MOBILEPHONE, FACEBOOK, GITHUB, LINKEDIN;

	}

	@Id
	@GeneratedValue
	private Long ID;

	private String content;
	
	private int orderOfView;
	
	@Enumerated(EnumType.STRING)
	private TypeInfo type;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_User")
	private User user;

	public UserInfo() {
		super();

	}

	public UserInfo(TypeInfo type, String content) {
		super();
		this.content = content;
		this.type = type;
	}

	public Long getID() {
		return this.ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public TypeInfo getType() {
		return type;
	}

	public void setType(TypeInfo type) {
		this.type = type;
	}

	public int getOrder() {
		return orderOfView;
	}

	public void setOrder(int order) {
		this.orderOfView = order;
	}

}