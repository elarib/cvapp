package com.elarib.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.elarib.entities.Component;

public interface ComponentRepository extends JpaRepository<Component, Long>{

}
