package com.elarib.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.elarib.entities.WorkExperience;

public interface WorkExperienceRepository extends JpaRepository<WorkExperience, Long>{

}
