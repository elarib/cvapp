package com.elarib.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.elarib.entities.Role;

public interface RolesRepository extends JpaRepository<Role, Long>{

}
