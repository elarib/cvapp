package com.elarib.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.elarib.entities.Competence;

public interface CompetenceRepository extends JpaRepository<Competence, Long>{

}
