package com.elarib.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.elarib.entities.ElementCompetence;

public interface ElementCompetenceRepository extends JpaRepository<ElementCompetence, Long>{

}
