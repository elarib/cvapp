package com.elarib.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.elarib.entities.User;
import com.elarib.entities.UserInfo;

@RepositoryRestResource(path="userInfos", collectionResourceRel="userInfos")
public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
	
	public UserInfo findByTypeAndUser(String type, User user);


}
