package com.elarib.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;

import com.elarib.entities.Objectif;
import com.elarib.entities.User;

//@PreAuthorize("hasRole('ROLE_USER'")
public interface ObjectifRepository extends JpaRepository<Objectif, Long> {
//
//	@Query("select c from Component where c.user.id = :user and c.name = 'objectif' ")
//	public Objectif getUserObjectif(@Param("user") Long user);
//	
	
	public Objectif findByNameAndUser(String name, User user);
	
	
	
}

