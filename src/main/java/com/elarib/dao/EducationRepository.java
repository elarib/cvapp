package com.elarib.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.elarib.entities.Education;

public interface EducationRepository extends JpaRepository<Education, Long>{

}
