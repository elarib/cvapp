package com.elarib.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.elarib.entities.Skill;

public interface SkillsRepository extends JpaRepository<Skill, Long>{

}
