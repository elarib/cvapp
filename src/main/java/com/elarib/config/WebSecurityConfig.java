package com.elarib.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
  
	
	@Autowired
	UserDetailsService userDetailsService;
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
//		super.configure(http);
		http.csrf().disable();
//		http.rememberMe()
//    	.key("remember")
//    	.rememberMeParameter("remember")
//    	.tokenValiditySeconds(3600);
		
		   http
            .authorizeRequests()
            	.antMatchers("/css/**" , 
            			"/img/**", 
            			"/js/**", 
            			"/fonts/**" , 
            			"/login" , 
            			"/register", 
            			"/registerSuccess").permitAll()
            	.anyRequest().authenticated()
                .and()
            .rememberMe()
            	.key("remember")
            	.rememberMeParameter("remember")
            	.tokenValiditySeconds(3600)
            	.and()
            .exceptionHandling().accessDeniedPage("/403")
                 .and()
	        .formLogin()
	             .loginPage("/login")
	             .passwordParameter("password")
	             .usernameParameter("email")
	             .failureUrl("/login?error")
	             .defaultSuccessUrl("/")
//	             .failureHandler(new AuthenticationFailureHandlerImpl())
	             .permitAll()
	             .and()
         .logout()
             .permitAll();

           
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//            .inMemoryAuthentication()
//                .withUser("user").password("password").roles("USER")
//                .and()
//        		.withUser("user1").password("password1").roles("TEST")
//        .and()
//		.withUser("user2").password("password2").roles("TEST").disabled(true);
		
        
        auth.userDetailsService(userDetailsService);
    }
}