package com.elarib.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.elarib.dao.EducationRepository;
import com.elarib.dao.ObjectifRepository;
import com.elarib.dao.SkillsRepository;
import com.elarib.dao.UserInfoRepository;
import com.elarib.dao.UserRepository;
import com.elarib.dao.WorkExperienceRepository;
import com.elarib.entities.Objectif;
import com.elarib.entities.User;
import com.elarib.service.Constant;

@Controller
@RequestMapping("/edit")
public class Edit {

	private static User user;

	@Autowired
	ObjectifRepository daoObjectif;

	@Autowired
	EducationRepository daoEducation;

	@Autowired
	WorkExperienceRepository daoWorkExperience;

	@Autowired
	SkillsRepository daoSkill;
	
	@Autowired
	UserRepository daoUser;
	
	@Autowired
	UserInfoRepository daoUserInfos;

	@RequestMapping("")
	public String editView(Model model, @RequestParam("c") String component, HttpServletRequest req) {

		user = (User) req.getSession().getAttribute("user");

		model.addAttribute("component", component);

		switch (component) {
		case Constant.OBJECTIF:
			if (daoObjectif.findByNameAndUser("objectif", user) == null) {
				model.addAttribute("objToSave", new Objectif());
			} else {
				model.addAttribute("objToSave", daoObjectif.findByNameAndUser(Constant.OBJECTIF, user));
			}
			break;
		case Constant.INFOS:	
			
			model.addAttribute("objToSave", user);
			break;
			
		case Constant.USER_INFOS:	
			
			
			break;
		default:
			break;
		}

		return "edit";
	}

	@RequestMapping(value = "/objectif", method = RequestMethod.POST)
	public String updateObjectif(Objectif objectif, Model model, HttpServletRequest req) {

		objectif.setUser(user);
		daoObjectif.save(objectif);

		return "redirect:/";
	}

	@RequestMapping(value = "/infos", method = RequestMethod.POST)
	public String updateInfo(User userToSave, Model model, HttpServletRequest req) {
		
		System.out.println(userToSave.getID());
		
		daoUser.save(userToSave);
		

		return "redirect:/";
	}
	
	
	



}


