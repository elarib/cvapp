package com.elarib.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.elarib.dao.ObjectifRepository;
import com.elarib.dao.RolesRepository;
import com.elarib.dao.UserInfoRepository;
import com.elarib.dao.UserRepository;
import com.elarib.entities.Objectif;
import com.elarib.entities.Role;
import com.elarib.entities.User;
import com.elarib.entities.UserInfo;
import com.elarib.entities.UserInfo.TypeInfo;
import com.elarib.service.Constant;

@Controller
public class LoginAndRegister {

	@Autowired
	UserRepository userDao;

	@Autowired
	RolesRepository roleDao;

	@Autowired
	UserInfoRepository userInfoDao;
	
	@Autowired
	ObjectifRepository objectifDao;

	@RequestMapping(value = "/login")
	public String login(HttpServletRequest req, Model model,
			@RequestParam(value = "error", required = false) String error) {
		AuthenticationException exception = (AuthenticationException) req.getSession()
				.getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
		if (exception != null) {
			model.addAttribute("error", true);
			model.addAttribute("exception", exception.getMessage());
		}
		model.addAttribute("newUser", new User());
		return "loginForm";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(User user) {
		System.out.println("In Register POST");
		System.out.println(user);
		userDao.save(user);

		// Assign ROLE_USER to the new user.
		Role role = new Role(Constant.ROLE_USER);
		role.setUser(user);
		roleDao.save(role);

		// Declare & Instantiate new 'Empty' user's Infos

		UserInfo email = new UserInfo(TypeInfo.EMAIL, user.getEmail());
		UserInfo mobile = new UserInfo(TypeInfo.MOBILEPHONE, "");
		UserInfo home = new UserInfo(TypeInfo.HOME, "");
		UserInfo linkedin = new UserInfo(TypeInfo.LINKEDIN, "");
		UserInfo fb = new UserInfo(TypeInfo.FACEBOOK, "");
		UserInfo github = new UserInfo(TypeInfo.GITHUB, "");
		UserInfo[] userInfos = { email, mobile, home, linkedin, fb, github };

		int i = 0;
		for (UserInfo info : userInfos) {
			info.setOrder(i);
			info.setUser(user);
			userInfoDao.save(info);
			i++;
		}
		
		// Create FIRST OBJECTIF !
		Objectif objectif = new Objectif(Constant.OBJECTIF);
		objectif.setUser(user);
		
		objectifDao.save(objectif);
		
		

		return "/registerSuccess";
	}

	@RequestMapping(value = "/register")
	public String registerForm() {

		return "/loginForm";
	}

	@RequestMapping(value = "/registerSuccess")
	public String registerSucess() {

		return "/registerSuccess";
	}

}
