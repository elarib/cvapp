package com.elarib.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.elarib.dao.UserRepository;
import com.elarib.entities.User;

@Controller
public class home {

	@Autowired
	UserRepository dao;

	@RequestMapping(value = "/")
	public String getHome(Model model, HttpServletRequest req) {

		req.getSession().setAttribute("user", getUserPrincipal());
		
		
		
		return "home";
	}

	@RequestMapping(value = "/403")
	public String error403(Model model, Principal user) {

		if (user != null) {
			model.addAttribute("msg", "Hi " + user.getName() + ", you do not have permission to access this page!");
		} else {
			model.addAttribute("msg", "You do not have permission to access this page!");
		}

		// model.addAttribute("error", error);
		return "403";
	}



	public User getUserPrincipal() {
		UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("***" + principal.getUsername());

		User user = dao.findByEmail(principal.getUsername());

		return user;

	}
	
	@RequestMapping(value = "/test")
	public String test(HttpServletRequest req)
	{
		
		req.getSession().setAttribute("user", getUserPrincipal());
		return "test";
	}
	
	
	 

}
