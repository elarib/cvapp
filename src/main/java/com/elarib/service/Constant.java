package com.elarib.service;

public interface Constant {
	
	String ROLE_USER="ROLE_USER";
	String OBJECTIF="objectif";
	String EDUCATION="education";
	String WORK_EXPERIENCE="work";
	String SKILLS="skills";
	String INFOS="infos";
	String USER_INFOS ="userInfos";

}
